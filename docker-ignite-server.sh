#!/bin/bash

sudo docker pull apacheignite/ignite:2.7.6

sudo docker container stop Ignite_1 Ignite_2
sudo docker container rm Ignite_1 Ignite_2

sudo docker run -d --name=Ignite_1 --net=host --mount type=bind,source="$(pwd)",target=/app -e "CONFIG_URI=/app/src/main/webapp/META-INF/config/server-config.xml" apacheignite/ignite:2.7.6
sleep 5s
sudo docker run -d --name=Ignite_2 --net=host --mount type=bind,source="$(pwd)",target=/app -e "CONFIG_URI=/app/src/main/webapp/META-INF/config/server-config.xml" apacheignite/ignite:2.7.6
sleep 5s

sudo docker container ls