<%@ page contentType="text/html; utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Success Page - Sample WebApp</title>
</head>
<body>

<c:choose>
    <c:when test="${sessionScope.user == null}">
        <c:redirect url="login.jsp"/>
    </c:when>

    <c:otherwise>
        <c:set var="user" value="${sessionScope.user}"/>
    </c:otherwise>
</c:choose>

<c:forEach var="cookieVal" items="${cookie}">
    <c:choose>
        <c:when test='${cookieVal.key == "user"}'>
            <c:set var="userName" value="${cookieVal.value.value}"/>
        </c:when>

        <c:when test='${cookieVal.key == "JSESSIONID"}'>
            <c:set var="sessionId" value="${cookieVal.value.value}"/>
        </c:when>
    </c:choose>
</c:forEach>
<h3>Hi <c:out value="${userName}"/>, Login successful. Your Session ID=<c:out value="${sessionId}"/>
</h3>
<br>
User=<c:out value="${user}"/>
<br>
<a href="checkout.jsp">Checkout Page</a>
<form action="${pageContext.request.contextPath}/logout" method="post">
    <input type="submit" value="Logout">
</form>
</body>
</html>