<%@ page contentType="text/html; utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login Page - Sample WebApp</title>
</head>

<body>

<c:if test="${sessionScope.user != null}">
    <c:redirect url="mainPage.jsp" />
</c:if>

<h2>Sign in</h2>

<c:if test="${requestScope.errorMessage != null}">
    <span style="color: red; font-style: italic">
        <c:out value="${requestScope.errorMessage}"/>
    </span>
</c:if>

<form action="${pageContext.request.contextPath}/login" method="post">
    <table>
        <tr>
            <th><label for="user-field">Username: </label></th>
            <td><input id="user-field" type="text" name="user"></td>
        </tr>

        <tr>
            <th><label for="pwd-field">Password: </label></th>
            <td><input id="pwd-field" type="password" name="pwd"></td>
        </tr>

        <tr>
            <td colspan="2">
                <button type="submit">Login</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
