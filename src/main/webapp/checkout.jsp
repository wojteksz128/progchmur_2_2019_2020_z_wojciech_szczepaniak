<%@ page contentType="text/html; utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Success Page - Sample WebApp</title>
</head>
<body>

<c:if test="${sessionScope.user == null}">
    <c:redirect url="login.jsp"/>
</c:if>

<c:forEach var="cookieVal" items="${cookie}">
    <c:if test='${cookieVal.key == "user"}'>
        <c:set var="userName" value="${cookieVal.value.value}"/>
    </c:if>
</c:forEach>

<h2>Hi <c:out value="${userName}" />, do the checkout.</h2>
<br>
<form action="${pageContext.request.contextPath}/logout" method="post">
    <input type="submit" value="Logout">
</form>
</body>
</html>