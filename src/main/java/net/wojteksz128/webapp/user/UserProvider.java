package net.wojteksz128.webapp.user;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class UserProvider {

    private static UserProvider instance;

    public static UserProvider getInstance() {
        if (instance == null) {
            instance = new UserProvider();
        }
        return instance;
    }

    private final List<User> users;

    private UserProvider() {
        users = Arrays.asList(
                User.builder().username("wojteksz128").password("qwerty").build(),
                User.builder().username("asia2405").password("azerty").build()
        );
    }

    public Optional<User> getUser(String username, String pwd) {
        return users.stream().filter(user -> user.getUsername().equals(username) && user.getPassword().equals(pwd)).findFirst();
    }
}
