package net.wojteksz128.webapp.servlet;

import net.wojteksz128.webapp.user.User;
import net.wojteksz128.webapp.user.UserProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static java.time.Duration.ofMinutes;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    public final UserProvider userProvider = UserProvider.getInstance();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("user");
        String pwd = request.getParameter("pwd");

        Optional<User> userOptional = userProvider.getUser(username, pwd);
        if (userOptional.isPresent()) {
            createSession(userOptional.get(), request, response);
        } else {
            showErrorIntoForm(request, response);
        }
    }

    private void createSession(User user, HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        int thirtyMinutesInSeconds = (int) ofMinutes(30).get(ChronoUnit.SECONDS);
        session.setMaxInactiveInterval(thirtyMinutesInSeconds);
        Cookie userName = new Cookie("user", user.getUsername());
        userName.setMaxAge(thirtyMinutesInSeconds);
        response.addCookie(userName);
        response.sendRedirect("mainPage.jsp");
    }

    private void showErrorIntoForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("errorMessage", "Either username or password is wrong.");
        getServletContext().getRequestDispatcher("/login.jsp").include(request, response);
    }
}
